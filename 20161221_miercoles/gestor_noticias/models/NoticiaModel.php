<?php  
//NoticiaModel.php
Class NoticiaModel{
	private $idNot;
	private $tituloNot;
	private $contenidoNot;
	private $autorNot;
	private $fechaNot;

	public function __construct($idNot, $tituloNot, $contenidoNot, $autorNot, $fechaNot){
		$this->idNot=$idNot;
		$this->tituloNot=$tituloNot;
		$this->contenidoNot=$contenidoNot;
		$this->autorNot=$autorNot;
		$this->fechaNot=$fechaNot;
	}
	public function dimeId(){
		return $this->idNot;
	}
	public function dimeTitulo(){
		return $this->tituloNot;
	}
	public function dimeContenido(){
		return $this->contenidoNot;
	}
	public function dimeAutor(){
		return $this->autorNot;
	}
	public function dimeFecha(){
		return $this->fechaNot;
	}
}


?>