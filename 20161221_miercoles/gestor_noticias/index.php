<?php  
require_once('config.php');
require_once('includes/conexion.php');
require_once('models/MasterModel.php');
?>
<!-- html:5>section>(header+nav+section+footer) -->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Gestor de Noticias</title>
</head>
<body>
	<section>
		<header>ENCABEZADO</header>
		<nav>
			<a href="index.php?p=noticias.php">Noticias</a>
			<a href="index.php?p=clientes.php">Clientes</a>
			<a href="index.php?p=noticias2.php">Noticias2</a>
		</nav>
		<section>
			<!-- CONTENIDO DEL CONTROLADOR -->
			<?php  
			if(isset($_GET['p'])){
				$p=$_GET['p'];
			}else{
				$p='noticias.php';
			}
			include('controllers/'.$p);
			?>
			<!-- FIN CONTENIDO DEL CONTROLADOR -->
		</section>
		<footer>PIE DE PAGINA</footer>
	</section>
</body>
</html>