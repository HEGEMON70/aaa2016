<?php  
//Incluyo mi modelo de noticia individual
include('models/NoticiaModel.php');
//Modelo de noticias
Class NoticiasModel{
	private $conexion;
	private $elementos;
	public function __construct(){
		$this->conexion=Conexion::conectar();
		$this->elementos=array();
	}
	public function listado(){
		$sql="SELECT * FROM noticias";
		$consulta=$this->conexion->query($sql);
		while($fila=$consulta->fetch_array()){
			$not=new NoticiaModel($fila['idNot'], $fila['tituloNot'], $fila['contenidoNot'], $fila['autorNot'], $fila['fechaNot']);
			$this->elementos[]=$not;
		}
		return $this->elementos;
	}
}
?>