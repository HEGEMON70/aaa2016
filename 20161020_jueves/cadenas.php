<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>cadenas.php</title>
</head>
<body>
	
	<?php 
	$c=" hola que tal estas  ";
	//Contar el numero de caracteres
	//strlen 
	echo strlen($c);
	echo '<br>';
	//Para eliminar espacios o saltos de linea de una cadena
	//nota: al principio y al final
	//trim, ltrim, rtrim
	$c=trim($c);
	echo $c;
	//Para extraer de una cadena de texto
	echo substr($c, 3, 5); //a que
	echo substr($c, -3, 2); //ta
	echo '<br>';

	//Para reemplazar cadenas dentro de cadena
	$c=str_replace("hola", "<b>hola</b>", $c);
	echo $c;

	//Para pasar cadenas a may o min (ucfirst y ucwords)
	echo strtolower($c);
	echo strtoupper($c);

	//Para trocear una cadena a través de un caracter
	$trozos=explode(" ", $c);
	for($i=0; $i<count($trozos); $i++){
		echo $trozos[$i];
		echo '<br>';
	}

	$cadena=implode(";", $trozos);
	echo $cadena;

	$html="<table border=1><tr><td>Hola</td></tr></table>";
	echo htmlentities($html);

	// < >&lt;    &gt;


	$vector=array("enero", "febrero", "marzo", "abril");

	//round -> redondear
	//floor -> redondea hacia abajo SI o SI
	//ceil -> redondea hacia arriba SI o SI

	for ($i=0; $i < count($vector); $i++) { 
		$letra=floor(strlen($vector[$i])/2);
		echo substr($vector[$i], $letra, 1);
	}

	// strlen
	// trim
	// substr
	// str_replace
	// strtolower
	// strtoupper
	// explode
	// implode
	// htmlentities

	//http://pastebin.com/DQYG6DDY

	?>



</body>
</html>