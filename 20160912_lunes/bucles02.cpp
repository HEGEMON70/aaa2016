//PROGRAMA bucles02
//Usando un bucle while, queremos que el programa nos pida
//numeros por pantalla, mientras que el numero sea superior a 0
//En cuanto pongamos un 0, deja de pedir numeros, y nos muestra 
//la suma de todos los numeros que hemos introducido

//FECHA 12 de Septiembre de 2016
#include <stdio.h>
int main(){
		int suma=0;
		int numero;
		float contador=0;
		float media=0;

		do{
			printf("Escribe un numero:");
			scanf("%d", &numero);
			fflush(stdin);
			suma+=numero;
			contador++;
		}while(numero!=0);
		contador--;

		media=suma/contador;

		printf("La suma es %d", suma);
		printf("La media es %.2f", media);
		//Pedimos un intro para Salir
		getchar();
}
