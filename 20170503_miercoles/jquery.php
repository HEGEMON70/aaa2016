<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>jquery1.php</title>
	<script src="jquery-3.2.1.min.js"></script>
	<script>
		var parrafos;

		$(document).ready(function(){

			// var parrafos=$('p');
			// parrafos.html('lo que sea');

			//$('p:odd').html('Hola que tal');
			//$('p:eq(2)').html('Hola que tal');
			//$('p').eq(2).html('Hola que tal');
			$('p').not(':eq(2)').html('Hola que tal');

			$('#content').find('h3').eq(2).html('nuevo texto para el tercer elemento h3').end().eq(0).html('nuevo texto para el primer elemento h3');

			// $('#content')
			//     .find('h3')
			//     .eq(0)
			//     .html('nuevo texto para el tercer elemento h3')
			//     .end() // reestablece la selección a todos los elementos h3 en #content
			//     .eq(0)
			//     .html('nuevo texto para el primer elemento h3');

			// $('#content')
			//     .find('h3')
			//     .eq(2)
			//     .html('nuevo texto para el tercer elemento h3');

			// $('#content')
			//     .find('h3')
			//     .eq(0)
			//     .html('nuevo texto para el primer elemento h3');

			// $('p').css('background-color','red'); //Una propiedad
			// $('p').css({'background-color':'green', 'border':'1px solid black'});
			//Añadir la clase
			$('p').addClass('clase1');
			//Quitar clase
			$('p').removeClass('clase1');
			//Si la tiene, la quita, y sino la pone
			$('p').toggleClass('clase1');

			// $('#imagen').attr('src','cesta.jpg');
			$('#imagen').attr({'src':'cesta.jpg','width':'50'});
		});
	</script>
	<style>
		.clase1{
			font-size: 30px;
			color: red;
		}
	</style>
</head>
<body>
	<img id="imagen" src="" alt="">

	<!-- p*3>lorem -->
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi quia placeat ab odit laudantium aliquid vitae eos officia, tenetur, porro animi, non. Reprehenderit eius officiis distinctio eveniet possimus repellat culpa!</p>
	<p>Nobis temporibus deserunt cum a tempore beatae quis perferendis fuga quam, magnam ipsam sapiente! Nobis esse officiis dolore porro rerum molestiae quam maiores, nihil, commodi. Nemo ducimus sunt deserunt similique.</p>
	<p>Incidunt deserunt sint, voluptatum nobis itaque, perspiciatis nulla ad earum eos saepe ab, molestiae necessitatibus ipsam blanditiis dolorum culpa? Ad iste corporis aspernatur, totam similique odit consectetur suscipit provident, quas.</p>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora numquam accusantium dolorem, exercitationem quasi omnis, eum fuga rerum quod culpa itaque maxime velit repellat. Ducimus recusandae magni rerum veritatis nesciunt.</p>
	<p>Voluptatibus provident, recusandae eveniet consectetur temporibus, consequatur sapiente harum pariatur dolor officiis voluptates corrupti aliquid doloremque aperiam impedit, id accusantium totam ullam reiciendis debitis vitae incidunt. Natus reprehenderit, recusandae debitis.</p>
	<p>Dignissimos assumenda adipisci ea hic similique reprehenderit porro libero, tenetur iste nulla nam perspiciatis magni omnis sed quaerat, minima praesentium. Amet voluptas iste, ratione dignissimos ducimus fuga vero voluptatibus sequi!</p>

	<div id="content">
		<h3>Lorem ipsum.</h3>
		<h3>Nam, quis!</h3>
		<h3>Neque, commodi.</h3>
		<h3>Officia, officiis.</h3>
		<h3>Ipsa, facilis!</h3>
		<h3>Quod, deserunt.</h3>
	</div>


</body>
</html>