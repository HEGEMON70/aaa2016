<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>jquery2.php</title>
	<script src="jquery-3.2.1.min.js"></script>
	<script>
		var elemento;
		var nombres=['elemento 1', 'elemento 2', 'elemento 3', 'elemento 4', 'elemento 5', 'elemento 6'];
		$(document).ready(function(){

			alert($('#uno').data('nombre'));

			$('#loading_spinner').hide();

			$('p').each(function(indice){
				$(this).data('nombre',nombres[indice]);
			});


			$('p').click(function(){
				//Para mandar mensajes a la consola
				//console.log('Has pinchado aqui');
				// $(this).removeClass('clase2');
				// $(this).addClass('clase1');
				//$(this).toggleClass('clase2');
				// $(this).fadeOut(600, function(){
				// 	$(this).html('Hoy es Miercoles. Gracias por pinchar');
				// });
				// $(this).fadeIn(600);

				elemento=$(this);

				var params = {"valor":'10',"nombre":$(this).data('nombre')};

				$.ajax({
					data:  params,
					url:   'datosajax.php',
					dataType: 'text',
					type:  'post',
					beforeSend: function () {
						//mostramos gif "cargando"
						$('#loading_spinner').show();
						//antes de enviar la petición al fichero PHP, mostramos mensaje
						//jQuery("#resultado").html("Déjame pensar un poco...");
					},
					success:  function (response) {
						//escondemos gif
						$('#loading_spinner').hide();
						//mostramos salida del PHP
						//jQuery("#resultado").html(response);
						elemento.html(response);
					}
				});


				alert($('#uno').data('nombre'));



			});

			// $('p').mouseover(function(){
			// 	$(this).toggleClass('clase2');
			// });
			// $('p').mouseout(function(){
			// 	$(this).toggleClass('clase2');
			// });


		});

	</script>
	<style>
		.clase1{
			font-size: 30px;
			color: red;
		}
		.clase2{
			border: 1px solid black;
			background-color:aquamarine;
		}
	</style>
</head>
<body>

	<div class="gifCarga"><img id="loading_spinner" src="squares.gif"></div>

	<!-- p.clase2*6>lorem -->
	<p class="clase2" data-nombre="hola" id="uno">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas, consequuntur beatae recusandae quisquam quae, vero incidunt ea porro repellendus amet voluptatem deleniti nemo facere! In quae soluta facilis reprehenderit, qui.</p>
	<p class="clase2">Corrupti autem veniam adipisci ex repellendus magnam. Velit qui fuga, maxime, dignissimos obcaecati id illo culpa optio provident asperiores architecto similique magni nisi officia sapiente sint tenetur sunt saepe nihil.</p>
	<p class="clase2">Sit totam dolorem maiores amet quod illo deserunt perferendis ipsam ipsa. Nisi ullam dignissimos dolorum dolorem odio placeat mollitia quis, vel minima voluptas error, facere ipsam voluptate, autem modi repudiandae?</p>
	<p class="clase2">Repellendus facilis velit quod quasi officia quaerat aliquid iure. Dolorum dolor at doloremque perspiciatis provident sunt fugiat consequuntur velit numquam id cupiditate laudantium, atque ut optio nemo facere magni iste!</p>
	<p class="clase2">Deserunt reprehenderit a rem, voluptate vel atque tenetur quibusdam odio? Porro, magni dignissimos doloribus nam atque nemo alias assumenda consequuntur praesentium excepturi facilis corrupti unde fugit provident? Ipsam, provident, obcaecati.</p>
	<p class="clase2">Voluptas odit explicabo facere maxime voluptatem obcaecati aspernatur itaque fugit nam eius, voluptatum molestiae sequi a officia earum rem sint maiores optio, excepturi soluta doloremque atque culpa repellat? Velit, harum?</p>
</body>
</html>