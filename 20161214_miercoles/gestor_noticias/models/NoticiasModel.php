<?php  
//Incluyo mi modelo de noticia individual
include('models/NoticiaModel.php');
//Modelo de noticias
Class NoticiasModel extends MasterModel{

	public function __construct(){
		parent::__construct('noticias');
		$this->campoOrden='idNot';
	}

	public function listado($numpag){
		$n=($numpag-1)*$this->elementosPorPagina;
		$sql="SELECT * FROM $this->tabla ORDER BY $this->campoOrden $this->orden LIMIT $n,$this->elementosPorPagina";
		$consulta=$this->conexion->query($sql);
		while($fila=$consulta->fetch_array()){
			$not=new NoticiaModel($fila['idNot'], $fila['tituloNot'], $fila['contenidoNot'], $fila['autorNot'], $fila['fechaNot']);
			$this->elementos[]=$not;
		}
		return $this->elementos;
	}
	
}
?>