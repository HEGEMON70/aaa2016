<?php  
require('modulos/menu/models/elementoModel.php');

Class MenuModel{
	private $elementos;
	public function __construct($nombres,$enlaces,$actual){
		$this->elementos=array();
		for($i=0;$i<count($enlaces);$i++){
			if($enlaces[$i]==$actual){
				$auxiliar=true;
			}else{
				$auxiliar=false;
			}
			$e=new ElementoModel($nombres[$i], $enlaces[$i], $auxiliar);
			$this->elementos[]=$e;
		}
	}
	public function dimeElementos(){
		return $this->elementos;
	}
}



?>