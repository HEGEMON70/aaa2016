<?php  
//Incluyo mi modelo de noticia individual
include('models/NoticiaModel.php');
//Modelo de noticias
Class NoticiasModel extends MasterModel{

	public function __construct(){
		parent::__construct('noticias');
		$this->campoOrden='idNot';
	}

	public function listado($numpag){
		$n=($numpag-1)*$this->elementosPorPagina;
		$sql="SELECT * FROM $this->tabla ORDER BY $this->campoOrden $this->orden LIMIT $n,$this->elementosPorPagina";
		$consulta=$this->conexion->query($sql);
		while($fila=$consulta->fetch_array()){
			$not=new NoticiaModel($fila['idNot'], $fila['tituloNot'], $fila['contenidoNot'], $fila['autorNot'], $fila['fechaNot']);
			$this->elementos[]=$not;
		}
		return $this->elementos;
	}

	public function verElemento($id){
		$sql="SELECT * FROM $this->tabla WHERE idNot=$id";
		$consulta=$this->conexion->query($sql);
		$fila=$consulta->fetch_array();
		$not=new NoticiaModel($fila['idNot'], $fila['tituloNot'], $fila['contenidoNot'], $fila['autorNot'], $fila['fechaNot']);
		return $not;
	}
	public function insertarElemento($tituloNot, $contenidoNot, $autorNot, $fechaNot){
		$sql="INSERT INTO $this->tabla (tituloNot, contenidoNot, autorNot, fechanot)VALUES('$tituloNot', '$contenidoNot', '$autorNot', '$fechaNot')";
		if($consulta=$this->conexion->query($sql)){
			return true;
		}else{
			return false;
		}
	}
}
?>