<?php 
// CRUD
// listado (Lista TODOS los productos de la bbdd)
// ver (Muestra informacion detallada del producto)
// insertar (Saca un formulario)
// insercion (Inserta los datos en la bbdd)
// modificar (Saca un formulario con todo rellenado)
// modificacion (Actualiza la bbdd)
// borrar (Borra el registro de la bbdd)

accion();

//////////////////////////////////////////
//COMIENZO DE LA FUNCION accion()
//////////////////////////////////////////
function accion(){
	//Recojo la accion que quiero realizar
	if(isset($_GET['accion'])){
		$accion=$_GET['accion'];
	}else{
		$accion='listado';
	}

	switch($accion){
		case 'ver':
			if(isset($_GET['id'])){
				$id=$_GET['id'];
				ver($id);
			}else{
				echo 'error al mostrar el producto';
			}
			break;
		case 'insertar':
			insertar();
			break;
		case 'insercion':
			insercion();
			break;
		case 'borrar':
			if(isset($_GET['id'])){
				$id=$_GET['id'];
				borrar($id);
			}else{
				echo 'error al borrar el producto';
			}
			break;
		case 'modificar':
			if(isset($_GET['id'])){
				$id=$_GET['id'];
				modificar($id);
			}else{
				echo 'error al modificar el producto';
			}
			break;
		case 'modificacion':
			if(isset($_GET['id'])){
				$id=$_GET['id'];
				modificacion($id);
			}else{
				echo 'error al modificar el producto';
			}
			break;
		case 'listado':
		default:
			listado();
			break;
	}
}
//////////////////////////////////////////
//FIN DE LA FUNCION accion()
//////////////////////////////////////////


//////////////////////////////////////////
//COMIENZO DE LA FUNCION listado()
//////////////////////////////////////////
function listado(){

	global $conexion;
	global $p;

	if(isset($_GET['orden'])){
		$orden=$_GET['orden'];
	}else{
		$orden='asc';
	}

	if(isset($_GET['campo'])){
		$campo=$_GET['campo'];
	}else{
		$campo='nombreProducto';
	}

	$sql="SELECT * FROM productos LEFT JOIN categorias ON productos.idCategoria=categorias.idCategoria ORDER BY $campo $orden";
	$consulta=$conexion->query($sql);

	//Calculo el numero de paginas
	$totalRegistros=$consulta->num_rows;
	$registrosPorPagina=10;
	$numeroDePaginas=ceil($totalRegistros/$registrosPorPagina);

	//Recojo el numero de pagina que quiero ver
	if(isset($_GET['nump'])){
		$nump=$_GET['nump'];
	}else{
		$nump=0;
	}

	$info='Mostrando página '.($nump+1).' de '.$numeroDePaginas.', ordenado por el '.$campo.', de forma '.$orden;

	?>

	<h2>Gestor de productos - <?php echo $info;?></h2>
	
	<h3>
		<a href="index.php?p=<?php echo $p;?>&accion=insertar">
			Alta de producto
		</a>
	</h3>

	<hr>
	<table>
		<thead>
			<tr>
				<td>Nombre (<a href="index.php?p=<?php echo $p;?>&orden=asc&campo=nombreProducto">A</a>/<a href="index.php?p=<?php echo $p;?>&orden=desc&campo=nombreProducto">D</a>)</td>

				<td>Categorias</td>

				<td>Precio (<a href="index.php?p=<?php echo $p;?>&orden=asc&campo=precioProducto">A</a>/<a href="index.php?p=<?php echo $p;?>&orden=desc&campo=precioProducto">D</a>) </td>
				<td>Acciones</td>
			</tr>
		</thead>
		<tbody>
			<?php 
			//Calculo la paginaInicial para la consulta
			$pagInicial=$nump*$registrosPorPagina;

			//Consulta para la paginacion
			$sql="SELECT * FROM productos LEFT JOIN categorias ON productos.idCategoria=categorias.idCategoria ORDER BY $campo $orden LIMIT $pagInicial,$registrosPorPagina";
			$consulta=$conexion->query($sql);

			//Recorro los resultados
			while($fila=$consulta->fetch_array()){
				
				if($fila['mostrarProducto']==1){
					$icono='icono-verde.png';
				}else{
					$icono='icono-rojo.png';
				}

				echo '<tr>';
				echo '<td>'.$fila['nombreProducto'].'<img src="imagenes/'.$icono.'" width="20"></td>';
				echo '<td>'.$fila['nombreCategoria'].'</td>';
				echo '<td>'.$fila['precioProducto'].'</td>';
				echo '<td><a href="index.php?p='.$p.'&accion=ver&id='.$fila['idProducto'].'">Ver</a> - <a href="index.php?p='.$p.'&accion=modificar&id='.$fila['idProducto'].'">Editar</a> - <a href="index.php?p='.$p.'&accion=borrar&id='.$fila['idProducto'].'" onClick="return confirm(\'¿Estas seguro?\')">Borrar</a></td>';
				echo '</tr>';
			}
			?>
		</tbody>
	</table>
	<hr>
	<?php 
	//Que no me deje pulsar en la pagina en la que estoy
	//Que si estoy ordenando por un campo y un orden
	//me mantenga dicho campo y dicho orden
	//Mostrar en algun sitio, el campo y orden
	for ($i=0; $i < $numeroDePaginas; $i++) { 
		if($nump==$i){
			echo ' '.($i+1).' ';
		}else{
			echo ' <a href="index.php?p='.$p.'&nump='.$i.'&orden='.$orden.'&campo='.$campo.'">'.($i+1).'</a> ';
		}
	}

}
//////////////////////////////////////////
//FIN DE LA FUNCION listado()
//////////////////////////////////////////


//////////////////////////////////////////
//COMIENZO DE LA FUNCION ver()
//////////////////////////////////////////
function ver($id){
	global $conexion;
	global $p;

	$sql="SELECT * FROM productos WHERE idProducto=$id";
	$consulta=$conexion->query($sql);
	if($fila=$consulta->fetch_array()){
		echo '<div>';
		echo '<p>'.$fila['nombreProducto'].'</p>';
		echo '<p>'.$fila['precioProducto'].'</p>';
		echo '<p>'.$fila['stockProducto'].'</p>';

		if(strlen($fila['imagenProducto'])>0){
			$imagen=$fila['imagenProducto'];
		}else{
			$imagen='imagen-no-disponible.png';
		}

		echo '<img src="imagenes/'.$imagen.'" width="300">';

		echo '</div>';
	}else{
		echo 'No existe ese producto';
	}

}
//////////////////////////////////////////
//FIN DE LA FUNCION ver()
//////////////////////////////////////////


//////////////////////////////////////////
//COMIENZO DE LA FUNCION insertar()
//////////////////////////////////////////
function insertar(){
	global $conexion;
	global $p;
	?>
	<form action="index.php?p=<?php echo $p;?>&accion=insercion" method="post" enctype="multipart/form-data">
		Nombre:<input type="text" name="nombreProducto"><br>
		Precio:<input type="text" name="precioProducto"><br>
		Stock:<input type="text" name="stockProducto"><br>
		Mostrar:<input type="checkbox" name="mostrarProducto" checked><br>
		Imagen:<input type="file" name="imagenProducto"><br>
		
		Categoria:
		<select name="idCategoria">
			<?php
			$sqlC="SELECT * FROM categorias ORDER BY nombreCategoria ASC";
			$consultaC=$conexion->query($sqlC);
			while($filaC=$consultaC->fetch_array()){
				echo '<option value="'.$filaC['idCategoria'].'">'.$filaC['nombreCategoria'].'</option>';
			}
			?>
		</select><br><br>
		<!-- TODO: AQUI LAS MARCAS -->
		<hr>
		<?php
		$sqlP="SELECT * FROM posologias";
		$consultaP=$conexion->query($sqlP);
		while($filaP=$consultaP->fetch_array()){
			echo '<input type="checkbox" value="'.$filaP['idPosologia'].'" name="idPosologia[]">'.$filaP['nombrePosologia'].'<br>';
		}
		?>
		<hr>
		<input type="submit" name="insertar" value="insertar">

	</form>
	<?php
}
//////////////////////////////////////////
//FIN DE LA FUNCION insertar()
//////////////////////////////////////////


//////////////////////////////////////////
//COMIENZO DE LA FUNCION insercion()
//////////////////////////////////////////
function insercion(){
	global $conexion;
	global $p;

	//Recojemos los datos por POST
	$nombreProducto=$_POST['nombreProducto'];
	$precioProducto=$_POST['precioProducto'];
	$stockProducto=$_POST['stockProducto'];
	$idCategoria=$_POST['idCategoria'];
	// TODO: ME falta coger el idMarca
	$idPosologia=$_POST['idPosologia'];

	if(isset($_POST['mostrarProducto'])){
		$mostrarProducto=1;
	}else{
		$mostrarProducto=0;
	}
	//$mostrarProducto=isset($_POST['mostrarProducto'])?1:0;
	if(is_uploaded_file($_FILES['imagenProducto']['tmp_name'])){
		$imagenProducto=time()."_".$_FILES['imagenProducto']['name'];
		move_uploaded_file($_FILES['imagenProducto']['tmp_name'], 'imagenes/'.$imagenProducto);
	}else{
		$imagenProducto='';
	}
	//Genero la consulta sql
	$sql="INSERT INTO productos(nombreProducto, precioProducto, stockProducto, imagenProducto, mostrarProducto, idCategoria)VALUES('$nombreProducto', $precioProducto, $stockProducto, '$imagenProducto', $mostrarProducto, $idCategoria)";

	//Ejecuto la consulta
	if($conexion->query($sql)){
		echo 'Ejecutado con exito';
		//Obtengo el ultimo ID insertado
		$idProducto=$conexion->insert_id;
		//Recorro un bucle
		for ($i=0; $i < count($idPosologia); $i++) { 
			$idP=$idPosologia[$i];
			$sqlP="INSERT INTO posologiasproductos(idProducto, idPosologia)VALUES($idProducto, $idP)";
			$conexion->query($sqlP);
		}

		header('Location:index.php?p='.$p.'&accion=listado');
	}else{
		echo 'Ha habido un problema';
	}

}
//////////////////////////////////////////
//FIN DE LA FUNCION insercion()
//////////////////////////////////////////


//////////////////////////////////////////
//COMIENZO DE LA FUNCION borrar()
//////////////////////////////////////////
function borrar($id){
	global $conexion;
	global $p;

	$sql="SELECT imagenProducto FROM productos WHERE idProducto=$id";
	$consulta=$conexion->query($sql);
	$fila=$consulta->fetch_array();
	if(strlen($fila['imagenProducto'])){
		unlink('imagenes/'.$fila['imagenProducto']);
	}

	$sql="DELETE FROM productos WHERE idProducto=$id";
	//Ejecuto la consulta
	if($conexion->query($sql)){
		echo 'Ejecutado con exito';
		header('Location:index.php?p='.$p.'&accion=listado');
	}else{
		echo 'Ha habido un problema';
	}
}
//////////////////////////////////////////
//FIN DE LA FUNCION borrar()
//////////////////////////////////////////


//////////////////////////////////////////
//COMIENZO DE LA FUNCION modificar()
//////////////////////////////////////////
function modificar($id){
	global $conexion;
	global $p;

	$sql="SELECT * FROM productos WHERE idProducto=$id";
	$consulta=$conexion->query($sql);
	if($fila=$consulta->fetch_array()){
		?>
		<form action="index.php?p=<?php echo $p;?>&accion=modificacion&id=<?php echo $id;?>" method="post" enctype="multipart/form-data">
			Nombre:<input type="text" name="nombreProducto" value="<?php echo $fila["nombreProducto"];?>"><br>
			Precio:<input type="text" name="precioProducto" value="<?php echo $fila["precioProducto"];?>"><br>
			Stock:<input type="text" name="stockProducto" value="<?php echo $fila["stockProducto"];?>"><br>

			<?php
			if($fila['mostrarProducto']==1){
				$che='checked';
			}else{
				$che='';
			}
			?>

			Mostrar:<input type="checkbox" name="mostrarProducto" <?php echo $che;?>><br>

			Imagen actual:<br>
			<?php
			if(strlen($fila['imagenProducto'])>0){
				$imagen=$fila['imagenProducto'];
			}else{
				$imagen='imagen-no-disponible.png';
			}

			echo '<img src="imagenes/'.$imagen.'" width="50">';
			?>
			<br>
			Cambiar imagen:
			<input type="file" name="imagenProducto"><br>

			
			Categoria:
			<select name="idCategoria">
				<?php
				$sqlC="SELECT * FROM categorias ORDER BY nombreCategoria ASC";
				$consultaC=$conexion->query($sqlC);
				while($filaC=$consultaC->fetch_array()){
					if($filaC['idCategoria']==$fila['idCategoria']){
						$sel='selected';
					}else{
						$sel='';
					}

					echo '<option value="'.$filaC['idCategoria'].'" '.$sel.'>'.$filaC['nombreCategoria'].'</option>';
				}
				?>
			</select><br><br>

			<input type="submit" name="modificar" value="modificar">
		</form>
		<?php
	}else{
		echo 'El producto no existe';
	}
}
//////////////////////////////////////////
//FIN DE LA FUNCION modificar()
//////////////////////////////////////////


//////////////////////////////////////////
//COMIENZO DE LA FUNCION modificacion()
//////////////////////////////////////////
function modificacion($id){
	global $conexion;
	global $p;

	//Recojemos los datos por POST
	$nombreProducto=$_POST['nombreProducto'];
	$precioProducto=$_POST['precioProducto'];
	$stockProducto=$_POST['stockProducto'];
	$idCategoria=$_POST['idCategoria'];

	if(isset($_POST['mostrarProducto'])){
		$mostrarProducto=1;
	}else{
		$mostrarProducto=0;
	}

	//A ver que hacemos con la imagen
	if(is_uploaded_file($_FILES['imagenProducto']['tmp_name'])){
		$imagenProducto=time()."_".$_FILES['imagenProducto']['name'];
		move_uploaded_file($_FILES['imagenProducto']['tmp_name'], 'imagenes/'.$imagenProducto);

		//Eliminamos la imagen anterior
		$sql="SELECT imagenProducto FROM productos WHERE idProducto=$id";
		$consulta=$conexion->query($sql);
		$fila=$consulta->fetch_array();
		if(strlen($fila['imagenProducto'])){
			unlink('imagenes/'.$fila['imagenProducto']);
		}

		//Genero la consulta sql
		$sql="UPDATE productos SET nombreProducto='$nombreProducto', precioProducto=$precioProducto, stockProducto=$stockProducto, mostrarProducto=$mostrarProducto, idCategoria=$idCategoria, imagenProducto='$imagenProducto' WHERE idProducto=$id";

	}else{
		//Genero la consulta sql
		$sql="UPDATE productos SET nombreProducto='$nombreProducto', precioProducto=$precioProducto, stockProducto=$stockProducto, mostrarProducto=$mostrarProducto, idCategoria=$idCategoria WHERE idProducto=$id";
	}

	//Ejecuto la consulta
	if($conexion->query($sql)){
		echo 'Ejecutado con exito';
		header('Location:index.php?p='.$p.'&accion=listado');
	}else{
		echo 'Ha habido un problema';
	}
}
//////////////////////////////////////////
//FIN DE LA FUNCION modificacion()
//////////////////////////////////////////
?>
