#include <stdio.h>
#include <windows.h>
#include <time.h>

int aleatorio(int min, int max){
	return rand()%(max-min+1)+min;
}

int main(){
		int T=400;
		int contador=0;
		srand(time(NULL));
		int numeros[T];
		for(int i=0; i<T; i++){
			numeros[i]=0;
		}
		for(int i=0; i<T; i++){
			numeros[i]=aleatorio(1, T);
			contador++;
			for(int j=0; j<i; j++){
				if(numeros[i]==numeros[j]){
					i--;
				}
			}
		}
		for(int i=0; i<T; i++){
			printf("%d ",numeros[i]);
		}
		printf("\r\n\r\n%d intentos", contador);
		getchar();
}
