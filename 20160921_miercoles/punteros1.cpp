#include <stdio.h>

//VAriables por REFERENCIA
void incrementa(int &a, int &b){
	a++;
	b++;
}

void intercambiar(int &a, int &b){
	int aux;
	aux=a;
	a=b;
	b=aux;
}

void intercambiar2(int *pa, int *pb){
    int aux;
    aux=*pa;
    *pa=*pb;
    *pb=aux;
    

}

int main(){
		int num=10;
		printf("%d \r\n", num);
		printf("%d  \r\n", &num);
        printf("%p  \r\n", &num);
        
        int *pnum;
        pnum=&num;
        
        printf("%d \r\n", pnum);
        printf("%p \r\n", pnum);
        printf("%d \r\n", *pnum);
        
        
        int vector[5]={8,5,3,1,23};
        int *pvector;
        pvector=vector;
        
        printf("%d \r\n", *(pvector+1));
        
        int a=10;
        int b=20;
        //incrementa(a,b);
        //intercambiar(a,b); //a=20 y b=10
        intercambiar2(&a,&b); //a=20 y b=10
        
        printf("\r\n\r\n%d %d", a, b);
        
		getchar();
}






