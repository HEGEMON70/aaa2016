<?php  

//Queremos consumir datos JSON desde una web que ofrezca datos JSON
$url='http://samples.openweathermap.org/data/2.5/forecast?lat=35&lon=139&appid=b1b15e88fa797225412429c1c50c122a1';
$json=file_get_contents($url);
//Decodificamos los datos, de json a variables de php
$datos=json_decode($json);

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
<div class="container">

<?php  


//Quiero sacar info del tiempo
$max=0;
$min=100;
for($i=0;$i<count($datos->list);$i++){
	if($max<$datos->list[$i]->wind->speed){
		$max=$datos->list[$i]->wind->speed;
	}
	if($min>$datos->list[$i]->wind->speed){
		$min=$datos->list[$i]->wind->speed;
	}
}

echo $max;
echo ' - ';
echo $min;
echo '<hr>';

for($i=0;$i<count($datos->list);$i++){
	$w=$datos->list[$i]->wind->speed;
	$por=$w*10;

	$por=(($w-$min)*100/($max-$min));

	?>
	<div class="progress">
	  <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $w*100;?>" aria-valuemin="0" aria-valuemax="1500" style="width: <?php echo $por;?>%;">
	    <?php echo $w;?> - <?php echo $datos->list[$i]->dt_txt; ?>  - <?php echo $por; ?>%
	  </div>
	</div>
	<?php


}



?>
</div>
</body>
</html>