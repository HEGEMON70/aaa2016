//Funcion para realizar una suma
int suma(int a, int b){
	int resultado=0;
	resultado=a+b;
	return resultado;
}

void dibujaMenu(){
	system("cls");
	printf("PROGRAMA CON MENU\r\n");
	printf("-----------------\r\n");
	printf("1.- Mostrar Vector\r\n");
	printf("2.- Ordenar Vector (Sust)\r\n");
	printf("3.- Ordenar Vector (Burbu)\r\n");
	printf("0.- Salir de la aplicacion\r\n");
}

int menu(){
	int opcion;
	dibujaMenu();
	printf("Elige una opcion: ");
	scanf("%d", &opcion);
	fflush(stdin);
	return opcion;
}

int aleatorio(){
	return (rand()%10)+1;
}

int aleatorio(int min, int max){
	int r;
	r=rand()%(max-min+1)+min;
	return r;
}