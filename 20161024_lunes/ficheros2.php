<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ficheros.php</title>
</head>
<body>
	<h2>Lector de ficheros de texto 2</h2>
	<?php 
	//Paso 1: Abrir el fichero
	$f=fopen('http://www.3djuegos.com/universo/rss/rss.php?plats=1-2-3-4-5-6-7-34&tipos=noticia-analisis-avance-video-imagenes-demo&fotos=no&limit=20', 'r');

	//Paso 2: Leer el fichero
	while ($linea=fgets($f)) {
		if(strpos($linea, 'title')!=false){
			echo htmlentities($linea);
			echo '<br>';
		}
		
	}

	//Paso 3: Cerrar el fichero
	fclose($f);

	?>
</body>
</html>