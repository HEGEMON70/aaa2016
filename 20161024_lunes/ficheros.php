<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ficheros.php</title>
</head>
<body>
	<h2>Lector de ficheros de texto</h2>
	<?php 
	//Paso 1: Abrir el fichero
	$f=fopen('CATALOGO_PUEBLOS.xls.csv', 'r');
	$f2=fopen('pueblos.txt', 'a');

	$contador=0;

	//Paso 2: Leer el fichero
	while ($linea=fgets($f)) {
		$trozos=explode(",", $linea);
		$nombre=$trozos[0];

		$valido=true;
		if(strlen($trozos[0])<2){
			$valido=false;
		}
		if(substr($trozos[0], 0, 1)=='-'){
			$valido=false;
		}
		if(strpos($trozos[0],'(continuación)')!=false){
			$valido=false;
		}
		if(trim($trozos[0]) == '(continúa)'){
			$valido=false;
		}
		if(trim($trozos[0]) == '(continuación)'){
			$valido=false;
		}
		if(strpos($trozos[0],'(continúa)')!=false){
			$nombre=substr($nombre, 0, strpos($trozos[0],'(continúa)'));
		}
		if($valido==true){
			fwrite($f2, $nombre."\r\n");
			echo $nombre;
			$contador++;
			echo '<br>';
		}
	}
	//Paso 3: Cerrar el fichero
	fclose($f);
	fclose($f2);

	//Muestro el numero de pueblos
	echo "<p>$contador</p>";
	?>
</body>
</html>