Lunes 27 de Marzo de 2017
-------------------------

Nos creamos un repositorio de bitbucket (dfb_plugin2)
	-> tipo de codigo PHP

	Nos creamos a mano la carpeta "dfb_plugin2"

	C:\xampp\htdocs\wordpress2\wp-content\plugins

	-----------------------

	1.- Creamos el repositorio en bitbucke
	2.- Creamos la carpeta local, e inicializamos el repositorio git
		
		git init

	3.- Añadimos el repositorio remoto de bitbucket

		git remote add origin https://davidfraj@bitbucket.org/davidfraj/dfb_plugin2.git

	4.- De forma local, añadimos los ficheros que hay dentro de mi directorio

		git add .

	5.- Realizamos el primer commit

		git commit -m "Mi primer commit"

	NOTA: El paso 4 y 5 se puede resumir en:

		git commit -am "Mi primer commit"

	6.- Subimos los cambios al repositorio REMOTo

		git push origin master

-----------------------------

Queremos hacer un plugin, para usar los metadatos de los posts de wordpress

	Datos que se pueden añadir directamente a un post

	tabla wp_posts
	tabla wp_postmeta

	Queremos que el usuario, ademas de añadir el contenido del post, asocie un video de youtube a cada post.

	Quiero añadir un meta(campo de texto) a mi gestor de posts


--------------------
<?php
/*
Plugin Name: Metadatos de video para mis posts
Plugin URI: http://www.davidfraj.com
Description: Descripcion del plugin
Version: 1.0
Author: DavidFraj
Author URI: http://www.davidfraj.com
License: 50% para David

*/

//mostramos una label para mi post
add_action('add_meta_boxes', 'dfb_add_metabox' );

//save metabox data
add_action('save_post', 'dfb_save_metabox' ); 

//register widgets QUE FUNCION VA A REGISTRAR EL WIDGET
add_action('widgets_init', 'dfb_widget_init');

function dfb_add_metabox() {
    //doc http://codex.wordpress.org/Function_Reference/add_meta_box
    add_meta_box('dfb_youtube', 'YouTube Video Link','dfb_youtube_handler', 'post');
}

function dfb_youtube_handler() {
    $value = get_post_custom($post->ID);
    $youtube_link = esc_attr($value['dfb_youtube'][0]);
    $youtube_link2 = esc_attr($value['dfb_youtube2'][0]);
    echo '<label for="dfb_youtube">YouTube Video Link</label><input type="text" id="dfb_youtube" name="dfb_youtube" value="'.$youtube_link.'" /><br>';
    echo '<label for="dfb_youtube2">Titulo Video Link</label><input type="text" id="dfb_youtube2" name="dfb_youtube2" value="'.$youtube_link2.'" />';
}

/**
 * save metadata
 */
function dfb_save_metabox($post_id) {
    //don't save metadata if it's autosave
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return; 
    }    
    
    //check if user can edit post
    if( !current_user_can( 'edit_post' ) ) {
        return;  
    }
    
    if( isset($_POST['dfb_youtube'] )) {
        update_post_meta($post_id, 'dfb_youtube', $_POST['dfb_youtube']);
    }
    if( isset($_POST['dfb_youtube2'] )) {
        update_post_meta($post_id, 'dfb_youtube2', $_POST['dfb_youtube2']);
    }
}

/**
 * register widget -> Funcion para registrar el Widget (llamada por add_action en la parte superior de nuestro plugin)
 */
function dfb_widget_init() {
    register_widget(Dfb_Widget);
}

//Tenemos que crear la nueva clase
/**
 * widget class
 */
class Dfb_Widget extends WP_Widget {
    function Dfb_Widget() {
        $widget_options = array(
            'classname' => 'dfb_class', //CSS
            'description' => 'Show a YouTube Video from post metadata'
        );
        
        $this->WP_Widget('dfb_id', 'YouTube Video', $widget_options);
    }
    
    /**
     * show widget form in Appearence / Widgets
     */
    function form($instance) {
        $defaults = array('title' => 'Video');
        $instance = wp_parse_args( (array) $instance, $defaults);
        
        $title = esc_attr($instance['title']);
        
        echo '<p>Title <input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
    }
    
    /**
     * save widget form
     */
    function update($new_instance, $old_instance) {
        
        $instance = $old_instance;        
        $instance['title'] = strip_tags($new_instance['title']);        
        return $instance;
    }
    
    /**
     * show widget in post / page
     */
    function widget($args, $instance) {
        extract( $args );        
        $title = apply_filters('widget_title', $instance['title']);
        
        //show only if single post
        if(is_single()) {
            echo $before_widget;
            echo $before_title.$title.$after_title;
            
            //get post metadata
            $dfb_youtube = get_post_meta(get_the_ID(), 'dfb_youtube', true);
             $dfb_youtube2 = get_post_meta(get_the_ID(), 'dfb_youtube2', true);
            
            //print widget content
            echo '<iframe width="200" height="200" frameborder="0" allowfullscreen src="http://www.youtube.com/embed/'.$dfb_youtube.'"></iframe>'; 

            echo $dfb_youtube2;      
            
            echo $after_widget;
        }
    }
}