<?php 
class Calendario{
	//Propiedades
	private $mes;
	private $anyo;
	private $meses;

	//Metodos
	//Metodo constructor de la clase
	public function __construct($anyo=0, $mes=0){
		if($anyo>0){
			$this->anyo=$anyo;
		}else{
			$this->anyo=date('Y');
		}

		if($mes>0){
			$this->mes=$mes;
		}else{
			$this->mes=date('n');
		}

		$this->meses=array("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
	}
	//Fin del constructor

	//Metodo para dibujar el mes del Calendario
	function dibujaMes($mes=0){

		if($mes==0){
			$mes=$this->mes;
		}

		$r='';
		$r.='<table border="1">';
		$r.='<tr><td colspan="7">'.$this->meses[$mes].' - '.$this->anyo.'</td></tr>';
		$r.='<tr>';
		$r.='<td>L</td>';
		$r.='<td>M</td>';
		$r.='<td>X</td>';
		$r.='<td>J</td>';
		$r.='<td>V</td>';
		$r.='<td>S</td>';
		$r.='<td>D</td>';
		$r.='</tr>';

		$finicio=mktime(0,0,0,$mes,1, $this->anyo);
		$dinicio=date('N', $finicio);
		//Si no estamos a lunes, rellenamos dias en blanco en el calendario
		if($dinicio!=1){
			$r.='<tr>';
			for ($i=1; $i < $dinicio; $i++) { 
				$r.='<td>&nbsp;</td>';
			}
		}

		$dultimo=date('t', $finicio);
		for ($dia=1; $dia <= $dultimo; $dia++) { 
			//Para cada dia del mes, genero una fecha en $f
			$f=mktime(0,0,0,$mes,$dia,$this->anyo);

			//Si es Lunes, abro fila
			if(date('N', $f)==1){
				$r.='<tr>';
			}

			//Dibujo el dia del mes es un td
			$r.='<td>'.$dia.'</td>';

			//Si es domingo, cierro fila
			if(date('N', $f)==7){
				$r.='</tr>';
			}
		}

		//Por si no he terminado en Domingo... cierro el tr final
		if(date('N', $f)!=7){
			$r.='</tr>';
		}

		$r.='</table>';

		return $r;

	}

	//Metodo para dibujar el año
	function dibujaAnyo(){
		$r='';
		$r.='<table border="1">';
		$r.='<tr><td colspan="3"><h2>'.$this->anyo.'</h2></td></tr>';
		for ($i=1; $i <= 12 ; $i++) { 
			if($i%3==1){
				$r.='<tr>';
			}
			$r.='<td>'.$this->dibujaMes($i).'</td>';
			if($i%3==0){
				$r.='</tr>';
			}
		}
		return $r;
	}



}



 ?>