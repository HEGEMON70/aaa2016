<?php 
//fichero includes/lista.class.php
class Lista{
	//Propiedades
	//$elementos, es un vector con los elementos
	private $elementos; //public, private, protected

	//Metodos
	//Metodo constructor, que recibe un vector
	function __construct($elementos){
		foreach ($elementos as $e) {
			$this->elementos[]=$e;
		}
	}

	//Metodo agregar, recibe un elemento, y lo
	//añade al vector $elementos
	public function agregar($elementos){
		if(is_array($elementos)){
			foreach ($elementos as $e) {
				$this->elementos[]=$e;
			}
		}else{
			$this->elementos[count($this->elementos)]=$elementos;
		}
	}
	//Metodo dibujar, no recibe nada, y devuelve 
	//el HTML para crear la lista desordenada (ul)
	function dibujar($tipo='des'){
		switch($tipo){
			case 'ord':
				$r='<ol>';
				break;
			default:
				$r='<ul>';
				break;
		}
		foreach ($this->elementos as $e) {
			$r.='<li>'.$e.'</li>';
		}
		switch($tipo){
			case 'ord':
				$r.='</ol>';
				break;
			default:
				$r.='</ul>';
				break;
		}
		return $r;
	}
}
?>