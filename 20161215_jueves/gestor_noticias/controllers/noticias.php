<?php  
//Archivo que llamara al modelo de datos, y cargara la vista
// controllers/noticias.php

include('models/NoticiasModel.php');
$modelo=new NoticiasModel();

//listado, ver, insertar, insercion, modificar, modificacion, borrar
if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='listado';
}

switch($accion){
	case 'listado':
		$modelo->elementosPorPagina(10); //opcional
		$modelo->estableceOrden('tituloNot', 'DESC');
		//Quiero recoger el numero de pagina por GET
		//Sera la pagina actual 1, 2, 3, 4
		if(isset($_GET['numpag'])){
			$numpag=$_GET['numpag'];
		}else{
			$numpag=1;
		}
		$elementos=$modelo->listado($numpag); //Saco las noticias
		$paginas=$modelo->dimePaginas(); //Saco cuantas paginas tengo
		include('views/NoticiasView.php');
		break;
	case 'ver':
		$elemento=$modelo->verElemento($_GET['id']);
		include('views/NoticiasVerView.php');
		break;
	case 'insertar':
		include('views/NoticiasInsertarView.php');
		break;
	case 'insercion':
		//Al recoger datos, los podemos comprobar.....
		$tituloNot=$_POST['tituloNot'];
		$contenidoNot=$_POST['contenidoNot'];
		$autorNot=$_POST['autorNot'];
		$fechaNot=$_POST['fechaNot'];
		$modelo->insertarElemento($tituloNot, $contenidoNot, $autorNot, $fechaNot);
		header('Location:index.php?p=noticias.php&accion=listado');
		break;
}
?>