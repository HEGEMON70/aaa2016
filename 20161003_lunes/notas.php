<?php 
	$nota1=6;
	$nota2=8;
	$nota3=9;
	// Con esta funcion, representamos la informacion con 2 decimales
	$media=number_format(($nota1+$nota2+$nota3)/3, 2);

	if($media>5){
		$calificacion="Aprobado";
	}else{
		$calificacion="Suspenso";
	}

	if(($media>=0)and($media<5)){
		$calificacion="Suspenso";
	}elseif(($media>=5)and($media<7)){
		$calificacion="Aprobado";
	}elseif(($media>=7)and($media<9)){
		$calificacion="Notable";
	}else{
		$calificacion="Sobresaliente";
	}

	$calificacion = ($media>5) ? "aprobado":"Suspenso";

	if($media>5){
		$calificacion="Aprobado";
	}else{
		$calificacion="Suspenso";
	}

	switch($media){
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
			$titulo="Has suspendido";
			break;
		case 5:
		case 6:
			$titulo="Has Aprobado";
			break;
		default:
			$titulo="Lo que sea";
			break;
	}
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Notas</title>
</head>
<body>
	<table width="400" border="1">
		<tr>
			<td>nota1</td>
			<td><?php echo $nota1; ?></td>
		</tr>
		<tr>
			<td>nota2</td>
			<td><?php echo $nota2; ?></td>
		</tr>
		<tr>
			<td>nota3</td>
			<td><?php echo $nota3; ?></td>
		</tr>
		<tr>
			<td>Media</td>
			<td><?php echo $media; ?></td>
		</tr>
		<tr>
			<td>Calificaicon</td>
			<td><?php echo $calificacion; ?></td>
		</tr>
	</table>

	<hr>

	<ul>
		<?php 
		for($i=0;$i<10;$i++){
			echo "<li>Fila $i</li>";
		}
		?>
	</ul>

	<ul>
		<?php for($i=0;$i<10;$i++){ ?>
			<li>Fila <?php echo $i; ?></li>
		<?php } ?>
	</ul>

</body>
</html>	