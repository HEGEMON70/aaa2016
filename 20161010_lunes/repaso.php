<?php
require_once("datos.php");

if(isset($_GET['c'])){
	$c=$_GET['c'];
}else{
	$c=0;
}

$menu='';
$menu.='<ul>';
for ($i=0; $i < count($capitulos); $i++) { 
	if($c!=$i){
		$clase='fondo';
	}else{
		$clase='';
	}
	$menu.='<li class="'.$clase.'" style="background-color:#'.$i.$i.$i.'">';
	if($c!=$i){
		$menu.='<a href="repaso.php?c='.$i.'">';
	}
	$menu.=$capitulos[$i];
	if($c!=$i){
		$menu.='</a>';
	}
	$menu.='</li>';
}
$menu.='</ul>';
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>repaso.php</title>
	<style type="text/css">
		.fondo{
			background-color: #A6CFEE;
		}
	</style>
</head>
<body>
<!-- <center>	
<table width="1000" border="1">
	<tr>
		<td width="200" valign="top">
			<?php echo $menu; ?>
		</td>
		<td width="800" valign="top">
			<?php echo $contenidos[$c]; ?>
		</td>
	</tr>
</table> -->

<div style="width:100%;">
	<div style="width:35%; display:inline-block; vertical-align:top;">
		<?php echo $menu; ?>
	</div>
	<div style="width:64%; display:inline-block; vertical-align:top;" class="fondo">
		<?php echo $contenidos[$c]; ?>
	</div>
</div>

</center>

</body>
</html>