<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>web01</title>
	<link rel="stylesheet" href="estilos.css">
</head>
<body>
	
	<!-- section>header+(section>(nav+div))+footer -->
	<section>
		<header><?php require_once("inc/encabezado.php"); ?></header>
		<section>
			<nav><?php require_once("inc/menu.php"); ?></nav>
			<div>
				<h2>Indice de la web</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis saepe placeat, omnis blanditiis commodi assumenda ea deleniti maxime. Quisquam voluptatibus illo facere voluptates quia nam esse deleniti animi doloribus nulla.</p>
				<p>Eum, suscipit dolor obcaecati ut est et consequatur fugit odio eveniet ratione doloribus eius sunt dignissimos pariatur cum autem voluptas, reprehenderit quae itaque, non qui? Recusandae beatae labore omnis minus?</p>
				<p>Tempora, quas! Quae laborum ipsum porro quidem eius, voluptates natus fuga, iusto. Sunt vitae eius praesentium accusamus voluptas. Dolore modi dolorum odio autem error voluptates consectetur blanditiis beatae, qui harum.</p>
				<p>Impedit, perspiciatis harum officiis! Repellendus ducimus quod enim perspiciatis quo quidem sint atque iusto odit id. Natus perspiciatis asperiores, molestiae, repellat veniam ab provident molestias doloribus, sapiente aperiam debitis deleniti?</p>

			</div>
		</section>
		<footer><?php require_once("inc/pie.php"); ?></footer>
	</section>

</body>
</html>