<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>web01</title>
	<link rel="stylesheet" href="estilos.css">
</head>
<body>
	
	<!-- section>header+(section>(nav+div))+footer -->
	<section>
		<header><?php require_once("inc/encabezado.php"); ?></header>
		<section>
			<nav><?php require_once("inc/menu.php"); ?></nav>
			<div>
				
				<h2>Noticias de la web</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae tenetur ullam, praesentium beatae sed provident itaque. Ut assumenda aliquam dolorem nihil ab. Sunt ducimus aliquid voluptatem. Asperiores veniam ipsam, dolore.</p>
				<p>Velit animi consequatur mollitia, esse magni iste, a totam dolores fuga tempora cumque quisquam quae omnis enim. Dolorum et id nam, aliquam eaque corporis, dolores atque, nulla quo, enim nostrum.</p>
				<p>Quaerat, nam adipisci iste porro accusamus minus nulla explicabo consequatur deleniti ex similique unde esse harum culpa. Esse tempore dolorum eius culpa, quos eum, officiis, doloremque nulla blanditiis neque sequi.</p>
				<p>Asperiores soluta, ipsa quam vel, magnam nam atque nihil ipsum voluptates veniam quod animi commodi, molestiae quasi repudiandae voluptate! Maxime assumenda maiores in quasi iusto consequatur molestias sit nisi vitae.</p>

			</div>
		</section>
		<footer><?php require_once("inc/pie.php"); ?></footer>
	</section>

</body>
</html>