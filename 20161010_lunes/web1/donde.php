<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>web01</title>
	<link rel="stylesheet" href="estilos.css">
</head>
<body>
	
	<!-- section>header+(section>(nav+div))+footer -->
	<section>
		<header><?php require_once("inc/encabezado.php"); ?></header>
		<section>
			<nav><?php require_once("inc/menu.php"); ?></nav>
			<div>
				
				<h2>Donde estamos</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae atque consectetur quod, commodi minima similique quibusdam soluta, cumque facere quaerat praesentium corporis maiores, autem deserunt fugit! Maiores, labore consequuntur totam.</p>
				<p>Asperiores, quas, alias. Facere libero quis nobis quaerat sunt reprehenderit esse id veritatis tempora voluptas, unde laboriosam, et praesentium, consectetur maxime. Fugiat vitae dicta cupiditate ad, saepe quam repellat delectus?</p>
				<p>Explicabo iusto quibusdam quas, labore totam suscipit laboriosam laborum molestias iste, perferendis similique adipisci repudiandae doloribus voluptatem minima assumenda ducimus aspernatur perspiciatis! Similique temporibus magni voluptatum explicabo, quidem excepturi quasi!</p>
				<p>Numquam ipsam qui rerum eveniet alias, neque distinctio doloremque, nostrum odit porro corrupti est at in illo ratione dolorum non vero tempore laborum, asperiores maiores explicabo. Quis in pariatur totam?</p>

			</div>
		</section>
		<footer><?php require_once("inc/pie.php"); ?></footer>
	</section>

</body>
</html>