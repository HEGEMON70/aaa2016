<?php 
if(isset($_GET['pagina'])){
	$pagina=$_GET['pagina'];
}else{
	$pagina="inicio.php";
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>web02</title>
	<link rel="stylesheet" href="estilos.css">
</head>
<body>
	
	<!-- section>header+(section>(nav+div))+footer -->
	<section>
		<header><?php require_once("inc/encabezado.php"); ?></header>
		<nav>Login de usuario y password</nav>
		<section>
			<nav><?php require_once("inc/menu.php"); ?></nav>
			<div>
				<?php require_once("paginas/$pagina"); ?>
			</div>
		</section>
		<footer><?php require_once("inc/pie.php"); ?></footer>
	</section>

</body>
</html>