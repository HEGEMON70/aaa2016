<?php 


function dibujaMes($mes, $anyo){


$meses=array("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

$r='';
$r.='<table border="1">';
$r.='<tr><td colspan="7">'.$meses[$mes].' - '.$anyo.'</td></tr>';
$r.='<tr>';
$r.='<td>L</td>';
$r.='<td>M</td>';
$r.='<td>X</td>';
$r.='<td>J</td>';
$r.='<td>V</td>';
$r.='<td>S</td>';
$r.='<td>D</td>';
$r.='</tr>';

$finicio=mktime(0,0,0,$mes,1, $anyo);
$dinicio=date('N', $finicio);
//Si no estamos a lunes, rellenamos dias en blanco en el calendario
if($dinicio!=1){
	$r.='<tr>';
	for ($i=1; $i < $dinicio; $i++) { 
		$r.='<td>&nbsp;</td>';
	}
}

$dultimo=date('t', $finicio);
for ($dia=1; $dia <= $dultimo; $dia++) { 
	//Para cada dia del mes, genero una fecha en $f
	$f=mktime(0,0,0,$mes,$dia,$anyo);

	//Si es Lunes, abro fila
	if(date('N', $f)==1){
		$r.='<tr>';
	}

	//Dibujo el dia del mes es un td
	$r.='<td>'.$dia.'</td>';

	//Si es domingo, cierro fila
	if(date('N', $f)==7){
		$r.='</tr>';
	}
}

//Por si no he terminado en Domingo... cierro el tr final
if(date('N', $f)!=7){
	$r.='</tr>';
}

$r.='</table>';

return $r;

}


function dimeAnyo($anyo){
	$r='';
	$r.='<table border="1">';
	$r.='<tr><td colspan="3"><h2>'.$anyo.'</h2></td></tr>';
	for ($i=1; $i <= 12 ; $i++) { 
		if($i%3==1){
			$r.='<tr>';
		}
		$r.='<td>'.dibujaMes($i, $anyo).'</td>';
		if($i%3==0){
			$r.='</tr>';
		}
	}
	return $r;
}

echo dimeAnyo(2016);


?>