<?php 	


function borrarRegistros($tabla, $campoId, $id){
	global $conexion;
	$sql="DELETE FROM $tabla WHERE $campoId=$id";
	if($conexion->query($sql)){
		return true;
	}else{
		return false;
	}
}

function insertarRegistros($valores, $tabla, $usarPK=false){
	global $conexion;

	$consulta=ejecutaSql("SELECT * FROM $tabla");
	$campos=array();
	foreach ($consulta->fetch_fields() as $key => $value) {
		$campos[]=$value->name;
	}

	if($usarPK==true){
		$ini=0;
	}else{
		$ini=1;
	}

	$sql="INSERT INTO $tabla(";
	for($i=$ini;$i<count($campos);$i++){
		if($i>$ini){
			$sql.=", ";
		}
		$sql.=$campos[$i];
	}
	$sql.=")VALUES(";
	for ($i=0; $i < count($valores); $i++) { 
		if($i>0){
			$sql.=", ";
		}
		$sql.="'".$valores[$i]."'";
	}
	$sql.=")";
	if($conexion->query($sql)){
		return true;
	}else{
		echo $sql;
		return false;

	}
}

function actualizarRegistros($valores, $tabla, $id){
	global $conexion;
	$consulta=ejecutaSql("SELECT * FROM $tabla");
	$campos=array();
	foreach ($consulta->fetch_fields() as $key => $value) {
		$campos[]=$value->name;
	}

	$sql="UPDATE $tabla SET ";
	for ($i=1; $i < count($campos); $i++) { 
		if($i>1){
			$sql.=', ';
		}
		$cam=$campos[$i];
		$val=$valores[$i-1];
		$sql.="$cam='$val'";

	}
	$camId=$campos[0];
	$sql.=" WHERE $camId=$id";

	if($conexion->query($sql)){
		return true;
	}else{
		echo $sql;
		return false;
	}
}


function ejecutaSql($sql){
	global $conexion;
	$consulta=$conexion->query($sql);
	return $consulta;
}

function contarRegistros($sql){
	$consulta=ejecutaSql($sql);
	return $consulta->num_rows;
}

function contarPaginas($sql, $registrosPorPagina=10){
	return ceil(contarRegistros($sql)/$registrosPorPagina);
}


?>