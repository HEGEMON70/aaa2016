#include <stdio.h>
#include <windows.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>

void mostrarVector(int *vector, int t){
	for(int i=0; i<t; i++){
		printf("%d ", vector[i]);
	}
}

void ordenarVector(int *vector, int t){
    //Ordeno el vector de resultados
	int aux=0;
	for(int i=0;i<t;i++){
		for(int j=i;j<t;j++){
			if(vector[i]>vector[j]){
				aux=vector[i];
				vector[i]=vector[j];
				vector[j]=aux;
			}
		}
	}
}
	
int main(){
	
	int vector[5]={5, 23, 87, 9, 12};
	printf("%d ", vector);
	printf("%p ", vector);
	printf("%d ", vector[0]);
	printf("%d ", vector[1]);

	int  *pvector;
	pvector=vector;
	printf("\r\n");
	printf("%d ", pvector);
	printf("%p ", pvector);
	printf("%d ", *(pvector+1));
	printf("%d ", pvector[1]);
	
	mostrarVector(vector,sizeof(vector)/sizeof(vector[0]));
	
	char nombre[]="Hola mundo";
	printf("Escribe el nuevo nombre: ");
	scanf("%s", nombre);
	fflush(stdin);
	
	printf("%c", nombre);
	
	strcpy(nombre, "Otro");

	for(int i=0; i<sizeof(nombre)/sizeof(nombre[0]); i++){
        printf("%c  ", nombre[i]);
	}

	getchar();
}
