#include <stdio.h>
#include <windows.h>
#include<stdlib.h>
#include<time.h>

int aleatorio(int min, int max){
	return (rand()%(max-min+1))+min;
}

void mostrarVector(int *vector, int t){
	for(int i=0; i<t; i++){
		printf("%d ", vector[i]);
	}
}
	
int main(){
	
	int dado1=0;
	int dado2=0;
	int tirada=0;
	float porcentaje;
	int resultados[11];
	int tiradas[11];
	srand(time(NULL));
	
	for(int i=0; i<11; i++){
		resultados[i]=0;
		tiradas[i]=i+2;
	}
	
	for(int i=0; i<350; i++){
		dado1=aleatorio(1,6);
		dado2=aleatorio(1,6);
		tirada=dado1+dado2;
		resultados[tirada-2]++;
	}
	
	//Ordeno el vector de resultados
	int aux=0;
	for(int i=0;i<11;i++){
		for(int j=i;j<11;j++){
			if(resultados[i]>resultados[j]){

				aux=resultados[i];
				resultados[i]=resultados[j];
				resultados[j]=aux;
				
				aux=tiradas[i];
				tiradas[i]=tiradas[j];
				tiradas[j]=aux;
				
			}
		}
	}

	for(int i=10; i>=0;i--){
		porcentaje=(float)resultados[i]/350*100;
		printf("%d -> %.2f \r\n", tiradas[i], porcentaje);
	}
	
	//Llamo a la funcion:
	printf("\r\nMuestro el vector desde la funcion: \r\n");
	mostrarVector(tiradas, 11);

	getchar();
}
