<?php 
session_start(); //Para usar variables de session $_SESSION['cesta'];
// $_SESSION['cesta']=array();
// $_SESSION['unidades']=array();

include('conexion.php');
if(isset($_GET['p'])){
	$p=$_GET['p'];
}else{
	$p='listadoProductos.php';
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Carrito de compra</title>
	<style>
		
		body{
			text-align: center;
		}
		section#contenedor{
			text-align: left;
			margin: 0px auto;
			width: 620px;
		}
		section#contenido{
			display:inline-block;
			width: 400px;
			vertical-align: top;
		}
		section#lateral{
			display:inline-block;
			width: 200px;
			vertical-align: top;
			border-left: 1px solid black;
			padding: 2px;
		}
		section#contenido article{
			padding: 10px;
			margin: 10px;
		}
		section#contenido article.detalle{
			font-size: 20px;
			padding: 10px;
			margin: 10px;
			border: 1px solid black;
		}
		section#contenido article.detalle p{
			font-size: 30px;
			font-weight: bold;
			margin: 5px;
		}

	</style>
</head>
<body>
	<section id="contenedor">
		<a href="index.html">index</a>
		<a href="contacto.html">contacto</a>
		<a href="donde-estamos.html">donde-estamos</a>
		<section id="contenido">
			<?php include($p); ?>
		</section>
		<section id="lateral">
			<?php include('carrito.php'); ?>
		</section>
	</section>
</body>
</html>
<?php 
	$conexion->close(); 
?>