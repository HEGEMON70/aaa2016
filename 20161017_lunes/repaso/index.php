<?php 
if(isset($_GET["p"])){
	$p=$_GET["p"];
}else{
	$p="capitulos.php";
}
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Web de GOT</title>
	<link rel="stylesheet" href="css/estilos.css">
</head>
<body>
	<section id="principal">
		<header><?php require("includes/encabezado.php");?></header>
		<nav><?php require("includes/menu.php");?></nav>
		<section><?php require("paginas/$p");?></section>
		<footer><?php require("includes/pie.php");?></footer>
	</section>
</body>
</html>