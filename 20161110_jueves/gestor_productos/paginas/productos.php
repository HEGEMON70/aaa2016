<?php 
if(isset($_GET['orden'])){
	$orden=$_GET['orden'];
}else{
	$orden='asc';
}

if(isset($_GET['campo'])){
	$campo=$_GET['campo'];
}else{
	$campo='nombreProducto';
}

$sql="SELECT * FROM productos ORDER BY $campo $orden";
$consulta=$conexion->query($sql);

//Calculo el numero de paginas
$totalRegistros=$consulta->num_rows;
$registrosPorPagina=10;
$numeroDePaginas=ceil($totalRegistros/$registrosPorPagina);

//Recojo el numero de pagina que quiero ver
if(isset($_GET['nump'])){
	$nump=$_GET['nump'];
}else{
	$nump=0;
}

$info='Mostrando página '.($nump+1).' de '.$numeroDePaginas.', ordenado por el '.$campo.', de forma '.$orden;

?>

<h2>Gestor de productos - <?php echo $info;?></h2>
<hr>
<table>
	<thead>
		<tr>
			<td>Nombre (<a href="index.php?p=productos.php&orden=asc&campo=nombreProducto">A</a>/<a href="index.php?p=productos.php&orden=desc&campo=nombreProducto">D</a>)</td>
			<td>Precio (<a href="index.php?p=productos.php&orden=asc&campo=precioProducto">A</a>/<a href="index.php?p=productos.php&orden=desc&campo=precioProducto">D</a>) </td>
			<td>Acciones</td>
		</tr>
	</thead>
	<tbody>
		<?php 
		//Calculo la paginaInicial para la consulta
		$pagInicial=$nump*$registrosPorPagina;

		//Consulta para la paginacion
		$sql="SELECT * FROM productos ORDER BY $campo $orden LIMIT $pagInicial,$registrosPorPagina";
		$consulta=$conexion->query($sql);

		//Recorro los resultados
		while($fila=$consulta->fetch_array()){
			echo '<tr>';
			echo '<td>'.$fila['nombreProducto'].'</td>';
			echo '<td>'.$fila['precioProducto'].'</td>';
			echo '<td>Ver - Editar - Borrar</td>';
			echo '</tr>';
		}
		?>
	</tbody>
</table>
<hr>
<?php 
//Que no me deje pulsar en la pagina en la que estoy
//Que si estoy ordenando por un campo y un orden
//me mantenga dicho campo y dicho orden
//Mostrar en algun sitio, el campo y orden
for ($i=0; $i < $numeroDePaginas; $i++) { 
	if($nump==$i){
		echo ' '.($i+1).' ';
	}else{
		echo ' <a href="index.php?p=productos.php&nump='.$i.'&orden='.$orden.'&campo='.$campo.'">'.($i+1).'</a> ';
	}
}
?>
