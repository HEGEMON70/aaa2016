<?php 
//Class AreaBase
class AreaBase{
	//Propiedades
	public $coordenadas;
	public $enlace;
	public static $contador=0;

	//Metodos

	public static function saluda(){
		echo 'Hola';
	}

	public function __construct($coordenadas, $enlace){
		$this->coordenadas=$coordenadas;
		$this->enlace=$enlace;
		self::$contador++;
	}
}
//Fin del AreaBase

//Class Rect
class Rect extends AreaBase{
	public function dibujar(){
		return '<area type="rect" coords="'.$this->coordenadas.'" href="'.$this->enlace.'">';
	}
}
//fin de Class Rect


class Circ extends AreaBase{
	public function dibujar(){
		return '<area type="circ" coords="'.$this->coordenadas.'" href="'.$this->enlace.'">';
	}
}

class Poly extends AreaBase{
	public function dibujar(){
		return '<area type="poly" coords="'.$this->coordenadas.'" href="'.$this->enlace.'">';
	}
}

$areas=array(new Circ('23,34,10', 'web1.html'), new Rect('23,34,67,129', 'web2.php'));

for ($i=0; $i < count($areas); $i++) { 
	echo $areas[$i]->dibujar();
}


echo AreaBase::$contador;
echo AreaBase::saluda();

echo '<hr>';

$a = new AreaBase('12,23', 'web1.php');
$b = clone $a;

echo $a->coordenadas;
echo '<br>';
echo $b->coordenadas;
echo '<br>';

$b->coordenadas='20,30';

echo $a->coordenadas;
echo '<br>';
echo $b->coordenadas;
echo '<br>';

?>