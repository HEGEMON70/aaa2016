<?php 
include('imagen.class.php');
include('area.class.php');
include('mapa.class.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>objetos.php</title>
</head>
<body>
	<?php 
		$img=new Mapa('koala.jpg', 'titulo', 'titulo_mapa');

		$img->addArea('rect', '20,25,84,113', 'web.php');
		$img->addArea('rect', '0,0,20,25', 'otro.php');

		echo $img->dibujar();
		
	 ?>
</body>
</html>