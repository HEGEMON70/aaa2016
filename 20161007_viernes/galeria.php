<?php 
//Recogemos la imagen que queremos mostrar
if(isset($_GET["imagen"])){
	$imagen=$_GET["imagen"];
}else{
	$imagen=1;
}
//Versión reducida del if de arriba.
$imagen=(isset($_GET["imagen"])) ? $_GET["imagen"]:1;


$descripcion=array("Gato con patos", "Gato acostao", "Gato tuerto", "Gatito", "Gato Cabreao");

//Generamos las imagenes en miniatura
// if($imagen>1){
// 	$imagenes='<a href="galeria.php?imagen='.($imagen-1).'">ANTERIOR</a>';
// }else{
// 	$imagenes='ANTERIOR';
// }

if($imagen>1){
	$imagenes='<a href="galeria.php?imagen='.($imagen-1).'">ANTERIOR</a>';
}else{
	$imagenes='<a href="galeria.php?imagen=5">ANTERIOR</a>';
}

for($i=1;$i<=5;$i++){
	if($imagen==$i){
		$imagenes.='<span style="border: 5px solid black; display:inline-block;">';
		$imagenes.='<img src="imagenes/'.$i.'.jpg" width="100" title="'.$descripcion[$i-1].'">';
		$imagenes.='</span>';
	}else{
		$imagenes.='<a href="galeria.php?imagen='.$i.'">';
		$imagenes.='<img src="imagenes/'.$i.'.jpg" width="100" title="'.$descripcion[$i-1].'">';
		$imagenes.='</a>';
	}
}

// if($imagen<5){
// 	$imagenes.='<a href="galeria.php?imagen='.($imagen+1).'">SIGUIENTE</a>';
// }else{
// 	$imagenes.='SIGUIENTE';
// }

if($imagen<5){
	$imagenes.='<a href="galeria.php?imagen='.($imagen+1).'">SIGUIENTE</a>';
}else{
	$imagenes.='<a href="galeria.php?imagen=1">SIGUIENTE</a>';
}


?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>galeria.php</title>
</head>
<body>
	<?php echo $imagenes;?>
	 <hr>
	 <img src="imagenes/<?php echo $imagen;?>.jpg" width="500" title="<?php echo $descripcion[$imagen-1];?>">
	 <p><?php echo $descripcion[$imagen-1];?></p>
</body>
</html>